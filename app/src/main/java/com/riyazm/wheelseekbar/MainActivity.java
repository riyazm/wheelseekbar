package com.riyazm.wheelseekbar;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();
    ImageView seekBar, seekBarBG;
    TextView label2;
    float initX;
    int screenWidth;
    private int TOUCH_PADDING = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = (ImageView) findViewById(R.id.seek_bar);
        seekBarBG = (ImageView) findViewById(R.id.seek_bar_bg);
        label2 = (TextView) findViewById(R.id.label2);
        label2.setText("50");
        final DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN:
                        initX = motionEvent.getX();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        float finalX = motionEvent.getX();
                        calculateAndSetup(finalX);
                        break;
                }
                return true;
            }
        });
    }

    private void calculateAndSetup(float finalX) {
        int progress = 0;

        // make sure to touch extreme limits 0 & 100
        if (initX < finalX) {
            progress = Math.round((finalX / screenWidth) * 100) + TOUCH_PADDING;
        } else {
            progress = Math.round((finalX / screenWidth) * 100) - TOUCH_PADDING;
        }

        if (progress >= 50 && progress <= 100) {
            label2.setText("" + progress);
            int color1 = Color.parseColor("#FF0097d6");
            int color2 = Color.parseColor("#FFff0000");
            float proportion = ((progress / 100f) - 0.5f) * 2;
            Log.d(TAG, "proportion : " + proportion);
            seekBar.setColorFilter(interpolateColor(color1, color2, proportion));
            seekBarBG.setColorFilter(interpolateColor(color1, color2, proportion));
        }
        initX = finalX;
    }

    private float interpolate(float a, float b, float proportion) {
        return (a + ((b - a) * proportion));
    }

    /**
     * Returns an interpoloated color, between from and to
     */
    private int interpolateColor(int from, int to, float ratio) {
        final float inverseRatio = 1f - ratio;
        final float r = Color.red(to) * ratio + Color.red(from) * inverseRatio;
        final float g = Color.green(to) * ratio + Color.green(from) * inverseRatio;
        final float b = Color.blue(to) * ratio + Color.blue(from) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }
}
